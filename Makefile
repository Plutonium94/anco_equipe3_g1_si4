#           Author: Damien Viano damien.viano06@gmail.com
#    Creation date: October 05 ,2013

TEX      = rubber
TEXFLAGS = --pdf
all: 
	$(TEX) $(TEXFLAGS) robot_simulator.tex

clean:
	rm -f *~
	$(TEX) --clean robot_simulator.tex

proper:
	rm -f *~
	$(TEX) --pdf --clean robot_simulator.tex 
